# Rendu TP1 - Docker



## But du projet

Le projet consiste en une plateforme Heimdall qui centralise l'ensemble des outils d'une entreprise


## Technologies

- Heimdall
- Swag
- redis
- nitter
- mariadb
- mysql
- wordpress
- phpmyadmin
- gitea
- postgres
- pgadmin
- nextcloud
- spacedeck
- mailcatcher 
- openldap
- phpldapadmin
- plik
